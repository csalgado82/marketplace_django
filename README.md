# TEST MARKETPLACE BEDU

Aplicación Python - Django para solucionar test 
Generación de un dema de un marketplace


## Requsitos

Para lograr replicar el proyecto en local es necesario contar con los siguientes requerimientos instalados:
- git
- python3
- venv


## Instalación

Clona el repositorio en tu local y ejecuta los siguientes comandos dentro del directorio del proyecto:

```bash
python3 -m venv venv
```

```bash
source venv/bin/activate
```

```bash
pip install -r requirements.txt
```

```bash
./manage.py migrate
```

Para iniciar el proyecto ejecuta el siguiente comando, reemplazando el puerto donde quieras que se inicie, ejemplo (./manage.py runserver 0.0.0.0:8000):

```bash
./manage.py runserver 0.0.0.0:PORT
```