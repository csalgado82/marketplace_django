from django.conf.urls import url, include
from django.contrib import admin

from products import urls as urls_products
from orders import urls as urls_orders

urlpatterns = [
    url(r'^api/v1/', include((urls_products, 'products'), namespace='products')),
    url(r'^api/v1/', include((urls_orders, 'orders'), namespace='orders')),
    url(r'^admin/', admin.site.urls),
]
