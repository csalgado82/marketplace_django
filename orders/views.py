import string
import random
from datetime import datetime

from rest_framework import serializers, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ParseError
from django.shortcuts import get_object_or_404
from django.db.models import Sum

from .serializers import (OrderCreateSerializer, OrderSerializer,
                          OrderUpdateSerializer, ProductOrderDeleteSerializer,
                          PaymentOrderSerializer)

from .models import ProductsOrder, Order
from products.models import Product


@api_view(['GET'])
def get_order(request, order_id):
    if request.method == 'GET':
        order = get_object_or_404(Order, order_id=order_id)
        order = OrderSerializer(order).data
        return Response(order, status=status.HTTP_200_OK)


@api_view(['POST'])
def create_order(request):
    if request.method == 'POST':
        data = request.data
        serializer = OrderCreateSerializer(data=data)
        
        if serializer.is_valid(raise_exception=True):
            products = serializer.validated_data.get('products', None)
            order = serializer.save()

            if products:
                for p in products:
                    product = get_object_or_404(Product, pk=p['product'])
                    subtotal = product.sale_price * p['quantity']
                    ProductsOrder.objects.create(
                        order=order,
                        product=product,
                        quantity=p['quantity'],
                        subtotal=subtotal
                    )
                total = ProductsOrder.objects.filter(order=order).aggregate(Sum('subtotal'))
                order.total = total['subtotal__sum']
                order.save()

            order = OrderSerializer(order).data

            return Response(order, status=status.HTTP_201_CREATED)


@api_view(['PUT', 'DELETE'])
def add_products_order(request, order_id):
    if request.method == 'PUT':
        data = request.data
        serializer = OrderUpdateSerializer(data=data)
        
        if serializer.is_valid(raise_exception=True):
            order = get_object_or_404(Order, order_id=order_id)
            products = serializer.validated_data
            
            for p in products['products']:
                try:
                    product = ProductsOrder.objects.get(
                        order=order,
                        product=p['product']
                    )
                    product.quantity = p['quantity']
                    product.subtotal = product.product.sale_price * p['quantity']
                    product.save()
                except ProductsOrder.DoesNotExist:
                    product = get_object_or_404(Product, pk=p['product'])
                    subtotal = product.sale_price * p['quantity']
                    ProductsOrder.objects.create(
                        order=order,
                        product=product,
                        quantity=p['quantity'],
                        subtotal=subtotal
                    )

            total = ProductsOrder.objects.filter(order=order).aggregate(Sum('subtotal'))
            order.total = total['subtotal__sum']
            order.save()

            order = OrderSerializer(order).data

            return Response(order, status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        data = request.data
        serializer = ProductOrderDeleteSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            order = get_object_or_404(Order, order_id=order_id)
            products = serializer.validated_data

            for p in products['products']:
                try:
                    product = ProductsOrder.objects.get(
                        order=order,
                        product=p
                    )
                    product.delete()
                except ProductsOrder.DoesNotExist:
                    raise NotFound(detail='No existe el producto con ID %s en la orden' % p)

            total = ProductsOrder.objects.filter(order=order).aggregate(Sum('subtotal'))
            order.total = total['subtotal__sum']
            order.save()

            order = OrderSerializer(order).data

            return Response(order, status=status.HTTP_200_OK)


@api_view(['POST'])
def payment_order(request, order_id):
    if request.method == 'POST':
        data = request.data
        serializer = PaymentOrderSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            order = get_object_or_404(Order, order_id=order_id)

            if order.payment_reference or order.paid_at:
                raise ParseError('La orden %s ya ha sido pagado' % order_id)

            chars = string.ascii_uppercase + string.digits
            order.payment_reference = ''.join(random.choice(chars) for _ in range(18))
            order.paid_at = datetime.now()
            order.save()

            order = OrderSerializer(order).data

            return Response(order, status=status.HTTP_200_OK)