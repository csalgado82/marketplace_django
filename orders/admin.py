from django.contrib import admin

from .models import (Order, ProductsOrder)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'order_id', 'custom_name',
        'total', 'status', 'created_at',
    ] 
    search_fields = [
        'order_id', 'custom_name', 'custom_email'
    ]
    list_filter = ['status']
    exclude = []

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            self.exclude = ['order_id', 'status']
        else:
            self.exclude = ['order_id']

        form = super(OrderAdmin, self).get_form(request, obj, **kwargs)

        return form


@admin.register(ProductsOrder)
class ProductsOrderAdmin(admin.ModelAdmin):
    list_display = [
        'get_order_id', 'get_product_name', 'quantity',
        'subtotal'
    ]

    def get_order_id(self, obj):
        if obj.order:
            return obj.order.order_id
        else:
            return ''

    get_order_id.short_description = 'Número de orden'
    get_order_id.allow_tags = True

    def get_product_name(self, obj):
        if obj.product:
            return obj.product.name
        else:
            return ''

    get_product_name.short_description = 'Producto'
    get_product_name.allow_tags = True