from django.urls import path

from .views import (create_order, get_order, add_products_order,
                    payment_order)


urlpatterns = [
    path('order/<str:order_id>/', get_order, name='get_order'),
    path('order/', create_order, name='create_order'),
    path('update_order/<str:order_id>/', add_products_order, name='update_order'),
    path('payment_order/<str:order_id>/', payment_order, name='payment_order')
]