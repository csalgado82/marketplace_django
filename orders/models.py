import string
import random

from django.db import models

from products.models import Product


ORDER_STATUS = (
    (1, 'Creada'),
    (2, 'Pagada'),
    (3, 'Cancelada')
)


class Order(models.Model):
    order_id = models.CharField('Número de orden', max_length=10)
    custom_name = models.CharField('Nombre del cliente', max_length=124)
    custom_email = models.EmailField('Email del cliente')
    total = models.DecimalField('Total', max_digits=14, decimal_places=2, default=0.0)
    status = models.IntegerField(choices=ORDER_STATUS)
    payment_reference = models.CharField('Referencia de pago', max_length=20, blank=True, null=True)
    paid_at = models.DateTimeField('Fecha de pago', blank=True, null=True)
    created_at = models.DateTimeField('Fecha de creación', auto_now_add=True)
    updated_at = models.DateTimeField('Fecha de actualización', auto_now=True)

    class Meta:
        verbose_name = 'Orden'
        verbose_name_plural = 'Ordenes'

    def __str__(self):
        return 'Orden - %s' % self.order_id

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.status:
            self.status = 1
            super().save(update_fields=['status'])
        if not self.order_id:
            chars = string.ascii_uppercase + string.digits
            self.order_id = ''.join(random.choice(chars) for _ in range(10))
            super().save(update_fields=['order_id'])


class ProductsOrder(models.Model):
    order = models.ForeignKey(Order, on_delete='cascade', related_name='products_order')
    product = models.ForeignKey(Product, on_delete='cascade', related_name='product_order')
    quantity = models.IntegerField('Cantidad')
    subtotal = models.DecimalField('Subtotal', max_digits=14, decimal_places=2)

    class Meta:
        verbose_name = 'Producto de la orden'
        verbose_name_plural = 'Productos de la orden'

    def __str__(self):
        return 'Producto de la orden %s' % self.order_id