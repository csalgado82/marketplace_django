from rest_framework import serializers

from products.serializers import ProductSerializer

from .models import Order, ProductsOrder


class ProductsOrderSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = ProductsOrder
        fields = ('product', 'quantity', 'subtotal')


class ProductOrderCreateSerializer(serializers.Serializer):
    product = serializers.IntegerField()
    quantity = serializers.IntegerField()


class OrderCreateSerializer(serializers.ModelSerializer):
    products = ProductOrderCreateSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            'custom_name', 'custom_email', 'products'
        )

    def create(self, validated_data):
        validated_data.pop('products')
        validated_data['status'] = 1

        return Order.objects.create(**validated_data)


class OrderUpdateSerializer(serializers.Serializer):
    products = ProductOrderCreateSerializer(many=True)


class OrderSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (
            'order_id', 'custom_name', 'custom_email', 'total', 'products',
            'status', 'payment_reference', 'paid_at', 'created_at'
        )

    def get_products(self, obj):
        value = None
        try:
            product_order = obj.products_order.all()
            value = ProductsOrderSerializer(product_order, many=True).data
        except Exception as e:
            pass

        return value


class ProductOrderDeleteSerializer(serializers.Serializer):
    products = serializers.ListField(child=serializers.IntegerField())


class PaymentOrderSerializer(serializers.Serializer):
    card = serializers.CharField()