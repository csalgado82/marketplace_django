from django.urls import path

from .views import GetProductList


urlpatterns = [
    path('products/', GetProductList.as_view(), name='get_products_list'),
]