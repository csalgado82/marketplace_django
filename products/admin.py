from django.contrib import admin

from .models import (ProductType, Product)


@admin.register(ProductType)
class ProductTypeAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'name', 'status'
    ] 
    search_fields = ['name', 'description']
    list_filter = ['status']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'name', 'type',
        'sku', 'sale_price', 'list_price',
        'status'
    ] 
    search_fields = ['name', 'description']
    list_filter = ['type', 'status']