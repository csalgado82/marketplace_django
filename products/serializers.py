from django.db import models
from django.db.models import fields
from rest_framework import serializers

from .models import Product, ProductType


class ProductTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductType
        fields = ('id', 'name', 'image')


class ProductSerializer(serializers.ModelSerializer):
    type = ProductTypeSerializer()
    
    class Meta:
        model = Product
        fields = (
            'id', 'name', 'description', 'type', 'image', 'sku', 'sale_price'
        )