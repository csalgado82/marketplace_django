from django.db import models


class ProductType(models.Model):
    name = models.CharField('Nombre', max_length=124)
    description = models.TextField('Descripción', blank=True, null=True)
    image = models.ImageField('Imagen', upload_to='types/')
    status = models.BooleanField('Estatus', default=True)
    created_at = models.DateTimeField('Fecha de creación', auto_now_add=True)
    updated_at = models.DateTimeField('Fecha de actualización', auto_now=True)

    class Meta:
        verbose_name = 'Tipo de producto'
        verbose_name_plural = 'Tipos de producto'

    def __str__(self):
        return 'Tipo - %s' % self.name


class Product(models.Model):
    name = models.CharField('Nombre', max_length=124)
    description = models.TextField('Descripción')
    type = models.ForeignKey(ProductType, on_delete='cascade', related_name='product_type')
    image = models.ImageField('Imagen', upload_to='products/')
    sku = models.CharField('SKU', max_length=16, unique=True)
    sale_price = models.DecimalField('Precio de venta', max_digits=12, decimal_places=2)
    list_price = models.DecimalField('Precio de lista', max_digits=12, decimal_places=2)
    status = models.BooleanField('Estatus', default=True)
    created_at = models.DateTimeField('Fecha de creación', auto_now_add=True)
    updated_at = models.DateTimeField('Fecha de actualización', auto_now=True)

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        return 'Producto - %s' % self.name