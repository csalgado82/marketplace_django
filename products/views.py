from rest_framework.generics import ListAPIView

from .models import Product
from .serializers import ProductSerializer


class GetProductList(ListAPIView):
    serializer_class = ProductSerializer
    model = Product

    def get_queryset(self):
        queryset = self.model.objects.filter(status=True)
        return queryset.order_by('-id')